use futures::future::try_join_all;
use std::collections::BTreeSet;
use std::collections::HashMap;
use std::error::Error;
use tokio_postgres::NoTls;

use backoff::future::retry_notify;
use backoff::ExponentialBackoff;
use log::{debug, info};
use postgis::{ewkb, Point};
use tokio_postgres as postgres;

use crate::constants;
use crate::postgres::queries;
use crate::types::{self, *};

pub(crate) struct OSMFilter {
    client: postgres::Client,
    address_exists_stmt: postgres::Statement,
    nysid_exists_stmt: postgres::Statement,
    street_exists_stmt: postgres::Statement,
    buildings_around_stmt: postgres::Statement,
    get_way_nodes_stmt: postgres::Statement,
    get_rel_members_stmt: postgres::Statement,
}

impl OSMFilter {
    pub(crate) async fn new(
        pg_connect: &str,
    ) -> Result<
        (
            OSMFilter,
            tokio::task::JoinHandle<Result<(), Box<dyn Error + Sync + Send>>>,
        ),
        postgres::Error,
    > {
        let (client, conn) = postgres::connect(pg_connect, NoTls).await?;

        let conn_task: tokio::task::JoinHandle<Result<(), Box<dyn Error + Sync + Send>>> =
            tokio::spawn(async move {
                match conn.await {
                    Ok(()) => Ok(()),
                    Err(e) => Err(Box::new(e) as Box<dyn Error + Sync + Send>),
                }
            });

        let address_exists_stmt = client.prepare(queries::ADDRESS_EXISTS);
        let nysid_exists_stmt = client.prepare(queries::ANY_NYS_IDS_EXIST);
        let street_exists_stmt = client.prepare(queries::STREET_EXISTS);
        let buildings_around_stmt = client.prepare(queries::BUILDINGS_AROUND);
        let get_way_nodes_stmt = client.prepare(queries::GET_WAY_NODES);
        let get_rel_members_stmt = client.prepare(queries::GET_REL_MEMBERS);

        let mut statements = try_join_all(vec![
            address_exists_stmt,
            nysid_exists_stmt,
            street_exists_stmt,
            buildings_around_stmt,
            get_way_nodes_stmt,
            get_rel_members_stmt,
        ])
        .await?;

        let get_rel_members_stmt = statements.pop().unwrap();
        let get_way_nodes_stmt = statements.pop().unwrap();
        let buildings_around_stmt = statements.pop().unwrap();
        let street_exists_stmt = statements.pop().unwrap();
        let nysid_exists_stmt = statements.pop().unwrap();
        let address_exists_stmt = statements.pop().unwrap();

        Ok((
            OSMFilter {
                client,
                address_exists_stmt,
                nysid_exists_stmt,
                street_exists_stmt,
                buildings_around_stmt,
                get_way_nodes_stmt,
                get_rel_members_stmt,
            },
            conn_task,
        ))
    }

    pub(crate) async fn filter_address_point(
        &self,
        address_point: &AddressPoint,
    ) -> Result<OSMDocument, Box<dyn Error + Send + Sync>> {
        let mut address_resp = self
            .retry_postgres_request(
                address_point,
                &self.address_exists_stmt,
                &[
                    &address_point.coords.0,
                    &address_point.coords.1,
                    &address_point.full_address_num(),
                    &address_point.street_name,
                ],
            )
            .await?;

        let find_equal_in_btree = |address_resp: &mut OSMDocument, key, val: &BTreeSet<_>| {
            let mut new_elts = Vec::new();

            for elt in &address_resp.elements {
                let tags = elt.get_tags();

                if let Some(osm_val) = tags.get(key) {
                    if *val
                        == osm_val
                            .split(";")
                            .map(String::from)
                            .collect::<BTreeSet<String>>()
                    {
                        new_elts.push(elt.clone());
                    }
                }
            }

            if new_elts.len() > 0 {
                address_resp.elements = new_elts;
            }
        };

        if let Some(ref point_rooms) = address_point.rooms {
            find_equal_in_btree(&mut address_resp, constants::KEY_ROOMS, point_rooms);
        }

        // if the address exists, but our address point has a unit, check if the thing with the address
        // also has the same unit
        if let Some(SubAddress::Unit(ref point_unit)) = address_point.sub_address {
            let mut elts_unit = Vec::new();

            for elt in &address_resp.elements {
                let tags = elt.get_tags();

                if let Some(osm_unit) = tags.get(constants::KEY_UNIT) {
                    if point_unit == types::parse_unit(osm_unit) {
                        elts_unit.push(elt.clone());
                    }
                }
            }

            if elts_unit.len() > 0 {
                address_resp.elements = elts_unit;
            }
        }

        if let Some(SubAddress::Flats(ref point_flats)) = address_point.sub_address {
            find_equal_in_btree(&mut address_resp, constants::KEY_FLATS, point_flats);
        }

        if let Some(ref floors) = address_point.floors {
            find_equal_in_btree(&mut address_resp, constants::KEY_FLOOR, floors);
        }

        if let Some(ref point_buildings) = address_point.buildings {
            find_equal_in_btree(&mut address_resp, constants::KEY_BUILDING, point_buildings);
        }

        // if our address point *does not* have a unit, flats, or floor, try to find a match that also
        // does not have these
        if address_point.sub_address.is_none() && address_point.floors.is_none() {
            let mut elts_primary = Vec::new();

            for elt in &address_resp.elements {
                let tags = elt.get_tags();

                if tags.get(constants::KEY_FLOOR).is_none()
                    && tags.get(constants::KEY_FLATS).is_none()
                    && tags.get(constants::KEY_UNIT).is_none()
                {
                    elts_primary.push(elt.clone());
                }
            }

            if elts_primary.len() > 0 {
                address_resp.elements = elts_primary;
            }
        }

        debug!("existing address response: {:#?}", address_resp);

        return Ok(address_resp);
    }

    pub(crate) async fn any_existing_nys_ids(
        &self,
        address_point: &AddressPoint,
    ) -> Result<bool, Box<dyn Error + Send + Sync>> {
        Ok(self
            .client
            .query_one(&self.nysid_exists_stmt, &[&address_point.nys_ids])
            .await?
            .get(0))
    }

    pub(crate) async fn buildings_around(
        &self,
        address_point: &AddressPoint,
        dist: f64,
    ) -> Result<OSMDocument, Box<dyn Error + Send + Sync>> {
        self.retry_postgres_request(
            address_point,
            &self.buildings_around_stmt,
            &[&address_point.coords.0, &address_point.coords.1, &dist],
        )
        .await
    }

    pub(crate) async fn street_exists_nearby(
        &self,
        address_point: &AddressPoint,
    ) -> Result<OSMDocument, Box<dyn Error + Send + Sync>> {
        self.retry_postgres_request(
            address_point,
            &self.street_exists_stmt,
            &[
                &address_point.coords.0,
                &address_point.coords.1,
                &address_point.street_name,
            ],
        )
        .await
    }

    pub(crate) async fn retry_postgres_request(
        &self,
        address_point: &AddressPoint,
        statement: &postgres::Statement,
        params: &'_ [&'_ (dyn postgres::types::ToSql + Sync)],
    ) -> Result<OSMDocument, Box<dyn Error + Send + Sync>> {
        let mut num_errors = 0;
        retry_notify(
            ExponentialBackoff::default(),
            || async {
                Ok(self
                    .postgres_request(&self.client, statement, params)
                    .await?)
            },
            |err, dur| {
                num_errors += 1;

                if num_errors >= 5 {
                    info!(
                        "{} errors during query for {:?} at {:?} error: {}",
                        num_errors, address_point, dur, err
                    );
                }
            },
        )
        .await
    }

    fn is_extra_tag(tag_name: &str) -> bool {
        tag_name == constants::EXTRA_TAG_CHANGESET
            || tag_name == constants::EXTRA_TAG_TIMESTAMP
            || tag_name == constants::EXTRA_TAG_UID
            || tag_name == constants::EXTRA_TAG_USER
            || tag_name == constants::EXTRA_TAG_VERSION
            || tag_name == constants::EXTRA_TAG_WAY_AREA
    }

    fn slim_tags_to_map(mut slim_tags: Vec<String>) -> HashMap<String, String> {
        let mut tags: HashMap<String, String> = HashMap::new();

        while slim_tags.len() > 0 {
            let value = slim_tags.pop().unwrap();
            let key = slim_tags.pop().unwrap();

            if !Self::is_extra_tag(&key) {
                tags.insert(key, value);
            }
        }

        tags
    }

    async fn postgres_request(
        &self,
        client: &postgres::Client,
        statement: &postgres::Statement,
        params: &'_ [&'_ (dyn postgres::types::ToSql + Sync)],
    ) -> Result<OSMDocument, backoff::Error<Box<dyn Error + Send + Sync>>> {
        let mut elements = Vec::new();

        debug!("sending query");

        for row in client.query(statement, params).await.map_err(|e| {
            debug!("error sending query: {}", e);
            backoff::Error::Transient(Box::new(e) as Box<dyn Error + Sync + std::marker::Send>)
        })? {
            let id: i64 = row.get(constants::COL_OSM_ID);
            let version: i32 = row.get(constants::COL_VERSION);
            let geometry: ewkb::GeometryT<ewkb::Point> = row.get(constants::COL_WAY);
            let element;

            // we can apparently see polygos that are relations. match on id parity as well
            // as geometry
            match geometry {
                ewkb::GeometryT::Point(p) => {
                    let lon = p.x();
                    let lat = p.y();
                    let tags: HashMap<String, String> = row
                        .get::<_, HashMap<String, Option<String>>>(constants::COL_TAGS)
                        .into_iter()
                        .map(|(k, v)| (k, v.unwrap_or(String::from(""))))
                        // take out extra tags that osm2pgsql inserts
                        .filter(|(k, _)| !Self::is_extra_tag(k))
                        .collect();

                    element = OSMElement::Node(Node {
                        id,
                        version: Some(version),
                        lat,
                        lon,
                        tags,
                    })
                }
                // can be either a multipolygon relation or a regular way
                ewkb::GeometryT::Polygon(_) | ewkb::GeometryT::LineString(_) if id >= 0 => {
                    let nodes_result = client
                        .query_one(&self.get_way_nodes_stmt, &[&id])
                        .await
                        .map_err(|e| {
                            backoff::Error::Transient(
                                Box::new(e) as Box<dyn Error + Sync + std::marker::Send>
                            )
                        })?;

                    let nodes: Vec<i64> = nodes_result.get(constants::COL_NODES);
                    let tags_raw = nodes_result.get::<_, Vec<String>>(constants::COL_TAGS);
                    let tags = Self::slim_tags_to_map(tags_raw);

                    element = OSMElement::Way(Way {
                        id,
                        version: Some(version),
                        nodes,
                        tags,
                    })
                }
                _ if id < 0 => {
                    // id is negative for relations
                    let id = id.abs();

                    let rel_members_row = client
                        .query_one(&self.get_rel_members_stmt, &[&id])
                        .await
                        .map_err(|e| {
                            backoff::Error::Transient(
                                Box::new(e) as Box<dyn Error + Sync + std::marker::Send>
                            )
                        })?;

                    let mut members_raw: Vec<String> = rel_members_row.get(constants::COL_MEMBERS);
                    let mut members = Vec::new();

                    while members_raw.len() >= 2 {
                        let role = members_raw.pop().unwrap();
                        let mut type_id = members_raw.pop().unwrap();
                        let refid: i64 = type_id.split_off(1).parse().map_err(|e| {
                            backoff::Error::Permanent(
                                Box::new(e) as Box<dyn Error + Sync + std::marker::Send>
                            )
                        })?;

                        let osm_type = match type_id.as_ref() {
                            "n" => String::from("node"),
                            "w" => String::from("way"),
                            "r" => String::from("relation"),
                            _ => {
                                return Err(backoff::Error::Permanent(
                                    format!("unknown type: {}", type_id).into(),
                                ))
                            }
                        };

                        members.insert(
                            0,
                            RelationMember {
                                refid,
                                osm_type,
                                role,
                            },
                        );
                    }

                    let tags_raw = rel_members_row.get::<_, Vec<String>>(constants::COL_TAGS);
                    let tags = Self::slim_tags_to_map(tags_raw);

                    element = OSMElement::Relation(Relation {
                        id,
                        version: Some(version),
                        members,
                        tags,
                    })
                }
                g @ _ => {
                    return Err(backoff::Error::Permanent(
                        format!("unhandled geometry: {:?}", g).into(),
                    ))
                }
            }

            elements.push(element);
        }

        Ok(OSMDocument { elements })
    }
}
