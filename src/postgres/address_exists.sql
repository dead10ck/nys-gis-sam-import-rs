-- look for any polygon or point with the same house number and street name

SELECT osm_id, osm_version, tags, way
FROM nysgissam_polygon
WHERE st_distancesphere(way, st_setsrid(st_makepoint($2, $1), 4326)) <= 300
  AND
    (SELECT count(*) > 0
     FROM regexp_split_to_table("addr:housenumber", e';') AS hn
     WHERE hn = $3)
  AND "addr:street" = $4
UNION ALL
SELECT osm_id, osm_version, tags, way
FROM nysgissam_point
WHERE st_distancesphere(way, st_setsrid(st_makepoint($2, $1), 4326)) <= 300
  AND
    (SELECT count(*) > 0
     FROM regexp_split_to_table("addr:housenumber", e';') AS hn
     WHERE hn = $3)
  AND "addr:street" = $4;
