-- look for any building near the point, and for any that are found, also include the relations
-- (other polygons) that it is a member of

SELECT osm_id,
       osm_version,
       tags,
       way
FROM nysgissam_polygon
WHERE st_dwithin(way::geography, st_setsrid(st_makepoint($2, $1), 4326)::geography, $3)
  AND ((building IS NOT NULL
        AND building != 'no')
       OR ("building:part" IS NOT NULL
           AND "building:part" != 'no'))
UNION ALL
SELECT osm_id,
       osm_version,
       tags,
       way
FROM nysgissam_polygon
WHERE osm_id = ANY
    (SELECT -rel_id
     FROM rel_members
     WHERE member_id = ANY
         (SELECT osm_id
          FROM nysgissam_polygon
          WHERE st_dwithin(way::geography, st_setsrid(st_makepoint($2, $1), 4326)::geography, $3)
            AND ((building IS NOT NULL
                  AND building != 'no')
                 OR ("building:part" IS NOT NULL
                     AND "building:part" != 'no'))));
