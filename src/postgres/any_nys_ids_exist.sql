SELECT count(*) > 0 AS nysid_exists
FROM nysids
WHERE nysid = ANY ($1);
