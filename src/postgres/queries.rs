pub(crate) const ADDRESS_EXISTS: &str = include_str!("address_exists.sql");
pub(crate) const ANY_NYS_IDS_EXIST: &str = include_str!("any_nys_ids_exist.sql");
pub(crate) const STREET_EXISTS: &str = include_str!("street_exists.sql");
pub(crate) const BUILDINGS_AROUND: &str = include_str!("buildings_around.sql");
pub(crate) const GET_WAY_NODES: &str = include_str!("get_way_nodes.sql");
pub(crate) const GET_REL_MEMBERS: &str = include_str!("get_rel_members.sql");

/*
#[macro_export]
macro_rules! any_nys_ids_exist {
    () => {
        r#"
[out:json];
nwr[~"^nysgissam:nysaddresspointid.*"~"({nys_ids_pattern})"];
out meta geom;
"#;
    };
}

#[macro_export]
macro_rules! street_exists {
    () => {
        r#"
[out:json];
way
  (around:1000,{lat},{lon})
  ["highway"]
  ["name"];
out meta geom;
"#;
    };
}

#[macro_export]
macro_rules! buildings_around {
    () => {
        r#"
[out:json];
(
    (
        way
        (around:300,{lat},{lon})
          [~"building.*"~".*"];
        -
        way
        (around:300,{lat},{lon})
          ["building"="no"];
    );
    <;
)->.a;
(
  way.a;
  relation.a["type"="multipolygon"];
);
out meta geom;
"#;
    };
}
*/
