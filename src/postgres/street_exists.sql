-- look for roads with the given name

SELECT osm_id,
       osm_version,
       tags,
       way
FROM nysgissam_line
WHERE st_distancesphere(way, st_setsrid(st_makepoint($2, $1), 4326)) <= 1000
  AND highway IS NOT NULL
  AND lower(regexp_replace(name, '''', '')) = lower(regexp_replace($3, '''', ''))
UNION ALL
SELECT osm_id,
       osm_version,
       tags,
       way
FROM nysgissam_roads
WHERE st_distancesphere(way, st_setsrid(st_makepoint($2, $1), 4326)) <= 1000
  AND highway IS NOT NULL
  AND lower(regexp_replace(name, '''', '')) = lower(regexp_replace($3, '''', ''));
