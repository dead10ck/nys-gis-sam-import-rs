mod constants;
mod filter;
mod osm;
mod postgres;
mod skip;
mod types;

use std::{
    cell::RefCell,
    collections::{BTreeSet, HashMap},
    error::Error,
    io, iter,
    ops::DerefMut,
    path::Path,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, Mutex,
    },
};

use clap::{App, Arg};
use futures::future::try_join_all;
use gdal::{
    spatial_ref::{CoordTransform, SpatialRef},
    vector::{sql, Geometry, Layer},
    Dataset,
};
use geo::algorithm::geodesic_distance::GeodesicDistance;
use log::{debug, error, info, LevelFilter};
use log4rs::{
    append::{
        console::{ConsoleAppender, Target},
        file::FileAppender,
    },
    config::{Appender, Config, Logger, Root},
    encode::pattern::PatternEncoder,
};
use ordinal::Ordinal;
use regex::{Captures, Regex};
use tokio::sync::mpsc;
use xml::writer::EmitterConfig;

use types::*;

const VERSION: &str = env!("CARGO_PKG_VERSION");
const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
const APP_NAME: &str = env!("CARGO_PKG_NAME");
const CRATE_NAME: &str = env!("CARGO_CRATE_NAME");

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let matches = App::new(APP_NAME)
        .version(VERSION)
        .author(AUTHORS)
        .about("Converts the NYS GIS Clearinghouse Address Point data to OSM")
        .arg(
            Arg::with_name("PG_CONNECT")
                .help("The connect string to the Postgres DB. See https://docs.rs/tokio-postgres/latest/tokio_postgres/config/struct.Config.html")
                .required(true)
                .short("c")
                .long("connect")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("NUM_FILTER_TASKS")
                .help("The number of simultaneous filter tasks to run")
                .short("t")
                .long("tasks")
                .takes_value(true)
                .default_value("10"),
        )
        .arg(
            Arg::with_name("LOG_LEVEL")
                .help("The log level")
                .short("l")
                .long("log-level")
                .takes_value(true)
                .possible_values(&["error", "warn", "info", "debug", "trace"])
                .default_value("info"),
        )
        .arg(
            Arg::with_name("SKIP_LOG_PATH")
                .help("The path to the skip log file")
                .short("s")
                .long("skip-log")
                .takes_value(true)
                .default_value("skip.log"),
        )
        .arg(
            Arg::with_name("NO_SKIP_IMPORTED")
                .help("If set, does not skip previously imported address points")
                .short("n")
                .long("no-skip-imported"),
        )
        .arg(
            Arg::with_name("BPOLY")
                .help("A space-separated list of coordinates to use as a bounding polygon")
                .short("b")
                .long("bpoly")
                .takes_value(true)
                .multiple(true)
                .allow_hyphen_values(true),
        )
        .arg(
            Arg::with_name("SQL_QUERY")
                .help("A SQL query filter to perform on the source data")
                .short("q")
                .long("sql")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("SQL_DIALECT")
                .help("The dialect of SQL")
                .short("d")
                .long("dialect")
                .takes_value(true)
                .possible_values(&["default", "ogr", "sqlite"])
                .requires("SQL_QUERY")
                .default_value("default")
        )
        .arg(
            Arg::with_name("SAM_ADDRESS_POINT_PATH")
                .help("The path to the SAM Adress Point data")
                .required(true),
        )
        .get_matches();

    let sam_address_point_path = matches.value_of("SAM_ADDRESS_POINT_PATH").unwrap();
    let pg_connect = matches.value_of("PG_CONNECT").unwrap();
    let skip_log_path = matches.value_of("SKIP_LOG_PATH").unwrap();
    let log_level: LevelFilter = matches.value_of("LOG_LEVEL").unwrap().parse().unwrap();
    let bpoly = matches.values_of("BPOLY").map(|values| {
        let values: Vec<_> = values.collect();
        assert!(values.len() % 2 == 0, "must give even number of numbers");
        values
            .chunks(2)
            .map(|chunk| {
                (
                    chunk[0].parse::<f64>().unwrap(),
                    chunk[1].parse::<f64>().unwrap(),
                )
            })
            .collect()
    });

    let sql_query = matches.value_of("SQL_QUERY");
    let sql_dialect = match matches.value_of("SQL_DIALECT").unwrap() {
        "default" => sql::Dialect::DEFAULT,
        "ogr" => sql::Dialect::OGR,
        "sqlite" => sql::Dialect::SQLITE,
        _ => unreachable!(),
    };

    let num_tasks: usize = matches
        .value_of("NUM_FILTER_TASKS")
        .unwrap()
        .parse()
        .unwrap();

    let no_skip_imported: bool = matches.is_present("NO_SKIP_IMPORTED");

    init_logging(skip_log_path, log_level)?;

    let dataset = Dataset::open(Path::new(sam_address_point_path))?;
    info!("Opened dataset in {}", sam_address_point_path);

    process(
        dataset,
        bpoly,
        sql_query,
        sql_dialect,
        pg_connect,
        num_tasks,
        no_skip_imported,
    )
    .await?;

    Ok(())
}

fn init_logging(
    skip_log_path: &str,
    log_level: LevelFilter,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let console_appender = ConsoleAppender::builder().target(Target::Stderr).build();

    let skip_appender = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new("{m}{n}")))
        .build(skip_log_path)
        .unwrap();

    let config = Config::builder()
        .appender(Appender::builder().build("console", Box::new(console_appender)))
        .appender(Appender::builder().build("skip", Box::new(skip_appender)))
        .logger(
            Logger::builder()
                .appender("skip")
                .additive(false)
                .build(format!("{}::skip", CRATE_NAME), LevelFilter::Info),
        )
        .build(Root::builder().appender("console").build(log_level))?;

    log4rs::init_config(config)?;

    Ok(())
}

async fn process(
    dataset: Dataset,
    bpoly: Option<Vec<(f64, f64)>>,
    sql_query: Option<&str>,
    sql_dialect: sql::Dialect,
    pg_connect: &str,
    num_tasks: usize,
    no_skip_imported: bool,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let mut points = Vec::new();

    let mut first_layer = dataset.layer(0).unwrap();

    let mut layer: Box<dyn DerefMut<Target = Layer>> = match sql_query {
        Some(sql_query) => Box::new(dataset.execute_sql(sql_query, None, sql_dialect)?.unwrap()),
        None => Box::new(&mut first_layer),
    };

    let ws84 = SpatialRef::from_epsg(4326)?;
    let layer_srs = layer.spatial_ref()?;
    let transform = CoordTransform::new(&layer_srs, &ws84)?;

    if let Some(bpoly) = bpoly.clone() {
        let wkt = format!(
            "POLYGON (({}))",
            bpoly
                .iter()
                .map(|coord| format!("{} {}", coord.0, coord.1))
                .collect::<Vec<_>>()
                .join(", ")
        );

        let mut filter = Geometry::from_wkt(&wkt)?;
        info!("applying spatial filter: {:?}", filter);

        filter.set_spatial_ref(ws84);
        filter.transform_to_inplace(&layer_srs)?;
        layer.set_spatial_filter(&filter);
    }

    for feature in layer.features() {
        if feature
            .field("PointType")?
            .ok_or("PointType field missing")?
            .into_string()
            .ok_or("PointType should be a string")?
            == "5"
        {
            continue;
        }

        let address_point = match AddressPoint::from_feature(feature, &transform) {
            Ok(ap) => ap,
            Err(e) => {
                error!("error parsing address point: {}", e);
                continue;
            }
        };

        debug!("parsed {:?}", address_point);
        points.push(RefCell::new(address_point));
    }

    info!("Read {} points. Stacking...", points.len());
    stack_close_points(&mut points);

    let pre_collapse_size = points.len();

    // discard address points that are duplicates in everything except location
    let grouped_by_location = AddressPoint::group_by_hasher(
        points.into_iter().map(RefCell::into_inner),
        AddressPoint::hash_all_except_location_no_coords,
    );

    let discarded_locations: Vec<_> = grouped_by_location
        .into_iter()
        .map(|(_, aps)| {
            let (mut with_location, without_location): (Vec<_>, Vec<_>) =
                aps.into_iter().partition(|ap| ap.location.is_some());

            // if there's exactly one without a location, just use that, discard the rest
            if without_location.len() == 1 {
                if with_location.len() > 0 {
                    debug!(
                        "discarding points: {:#?}\nkeeping: {:#?}",
                        with_location, without_location
                    );
                }
                without_location

            // otherwise combine again
            } else {
                with_location.extend(without_location);
                with_location
            }
        })
        .flatten()
        .collect();

    let num_discards = pre_collapse_size - discarded_locations.len();

    let grouped_by_building =
        AddressPoint::group_by_hasher(discarded_locations, AddressPoint::hash_all_except_building);

    let collapsed_buildings = collapse_ap_btreeset(
        grouped_by_building,
        |ap| &mut ap.buildings,
        |ap| ap.buildings,
    );
    let grouped_by_sub_address = AddressPoint::group_by_hasher(
        collapsed_buildings,
        AddressPoint::hash_all_except_sub_address,
    );
    let collapsed_sub_addresses = collapse_units(grouped_by_sub_address);
    let grouped_by_rooms =
        AddressPoint::group_by_hasher(collapsed_sub_addresses, AddressPoint::hash_all_except_room);
    let collapsed_rooms = collapse_ap_btreeset(grouped_by_rooms, |ap| &mut ap.rooms, |ap| ap.rooms);

    // group by all except room and unit and absorb floor with floor + unit/room
    let grouped_except_rooms_units =
        AddressPoint::group_by_hasher(collapsed_rooms, AddressPoint::hash_all_except_room_unit);
    let collapsed_solitary_floors = absorb_single_floors_with_units(grouped_except_rooms_units);
    let grouped_by_floor = AddressPoint::group_by_hasher(
        collapsed_solitary_floors,
        AddressPoint::hash_all_except_floor,
    );
    let collapsed_floors =
        collapse_ap_btreeset(grouped_by_floor, |ap| &mut ap.floors, |ap| ap.floors);

    let grouped_by_primary_address = AddressPoint::group_by_hasher(
        collapsed_floors,
        AddressPoint::hash_by_coord_primary_address,
    );
    let collapsed = collapse_sub_address_clusters(grouped_by_primary_address);

    let num_points = collapsed.len();
    let processed_ctr = Arc::new(AtomicUsize::new(0));
    let new_ctr = Arc::new(AtomicUsize::new(0));
    let mod_ctr = Arc::new(AtomicUsize::new(0));
    let skip_ctr = Arc::new(AtomicUsize::new(0));

    let (unfiltered_tx, unfiltered_rx) = async_channel::bounded(1000);
    let (conflater_tx, mut conflater_rx) = mpsc::channel(1000);
    let (xml_writer_tx, mut xml_writer_rx) = mpsc::channel(1000);

    info!(
        "Read {} address points, discarded {}, combined to {}. Beginning processing.",
        pre_collapse_size, num_discards, num_points
    );

    let send_task: tokio::task::JoinHandle<Result<(), Box<dyn Error + Sync + Send>>> =
        tokio::spawn(async move {
            for address_point in collapsed {
                unfiltered_tx.send(address_point).await?
            }

            Ok(())
        });

    let filter_tasks: Vec<tokio::task::JoinHandle<Result<(), Box<dyn Error + Sync + Send>>>> =
        iter::repeat_with(|| {
            let unfiltered_rx = unfiltered_rx.clone();
            let xml_writer_tx = xml_writer_tx.clone();
            let conflater_tx = conflater_tx.clone();
            let processed_ctr = processed_ctr.clone();
            let new_ctr = new_ctr.clone();
            let skip_ctr = skip_ctr.clone();
            let pg_connect = String::from(pg_connect);

            tokio::spawn(async move {
                let (osm_filter, conn_task) = filter::OSMFilter::new(&pg_connect).await?;
                let numbered_pattern = Regex::new(constants::NUMBERED_STREET_PATTERN).unwrap();

                while let Ok(mut address_point) = unfiltered_rx.recv().await {
                    if !no_skip_imported {
                        let existing_nys_ids =
                            osm_filter.any_existing_nys_ids(&address_point).await?;

                        if existing_nys_ids {
                            skip_ctr.fetch_add(1, Ordering::Relaxed);
                            skip::log_skip(&address_point);
                            debug!("Found existing NYS ID: {:#?}", address_point.nys_ids);
                            processed_ctr.fetch_add(1, Ordering::Relaxed);
                            continue;
                        }
                    }

                    debug!(
                        "filtering for existing street for '{}'",
                        address_point.street_name
                    );
                    let mut filter_doc = osm_filter.filter_address_point(&address_point).await?;

                    //filter_doc.elements = filter_doc
                    //.elements
                    //.into_iter()
                    //.filter(|elt| !elt.get_tags().contains_key(constants::KEY_NYS_ID))
                    //.collect();

                    //info!("filter_doc.elements: {:#?}", filter_doc.elements);

                    debug!(
                        "looking for existing street for '{}'",
                        address_point.street_name
                    );

                    let mut street_exists_doc = if address_point.has_excluded_street() {
                        debug!("Excluded street: {}", &address_point.street_name);
                        None
                    } else {
                        Some(osm_filter.street_exists_nearby(&address_point).await?)
                    };

                    debug!(
                        "found existing street for '{}': {:?}",
                        address_point.street_name, street_exists_doc
                    );

                    // if there is:
                    //
                    // 1. no existing address found
                    // 2. no nearby street
                    // 3. the address street name matches the numbered street pattern
                    // 4. the street exists nearby if you insert the ordinal
                    //
                    // then update the street name in the address point and try it again

                    let num_matches = filter_doc.elements.len();
                    let mut change = None;

                    if let Some(ref street_doc) = street_exists_doc {
                        if street_doc.elements.len() == 0 && num_matches == 0 {
                            let with_ordinal = numbered_pattern.replace(
                                &address_point.street_name,
                                |caps: &Captures| {
                                    let num: usize = caps.get(0).unwrap().as_str().parse().unwrap();
                                    Ordinal(num).to_string()
                                },
                            );

                            let mut ap_with_ord = address_point.clone();
                            ap_with_ord.street_name = with_ordinal.to_string();

                            let street_with_ord =
                                osm_filter.street_exists_nearby(&ap_with_ord).await?;

                            // if we found a street nearby with the ordinal
                            // replacement, then replace the street with it no
                            // matter what and try the filtering again
                            if street_with_ord.elements.len() > 0 {
                                debug!("found street with ordinal replacement: '{}'", with_ordinal);
                                address_point.street_name = ap_with_ord.street_name;
                                street_exists_doc = Some(street_with_ord);
                            }

                            filter_doc = osm_filter.filter_address_point(&address_point).await?;
                        }
                    }

                    // if there is more than one match, that can be okay, but it should not get
                    // conflated. If all the address tags are unique, then we don't need to mark
                    // for review
                    if num_matches > 1 {
                        // don't filter out because of other imported addresses
                        let non_nys_matches = filter_doc
                            .elements
                            .iter()
                            .filter(|elt| !elt.get_tags().contains_key(constants::KEY_NYS_ID))
                            .count();

                        let mut node = Node::new(0, address_point);

                        // if there are multiple matches of other imported addresses, they did not
                        // conflate before, so if we're adding new imported points, assume they
                        // can't conflate and just add them
                        if non_nys_matches > 1 {
                            node.mark_for_review("found > 1 existing matching address");
                        }

                        debug!("address_point: {:#?}, matches: {:#?}", node, filter_doc);
                        change = Some(types::Change::New(node));
                        new_ctr.fetch_add(1, Ordering::Relaxed);
                    } else if num_matches == 1 {
                        // send to conflater
                        conflater_tx
                            .send((
                                address_point,
                                ConflateEvent::FoundMatch(filter_doc.elements.pop().unwrap()),
                                street_exists_doc,
                            ))
                            .await?;
                    } else {
                        let mut nearby_buildings =
                            osm_filter.buildings_around(&address_point, 0.).await?;

                        if nearby_buildings.elements.len() <= 0 {
                            nearby_buildings = osm_filter
                                .buildings_around(&address_point, constants::NEARBY_MAX_DISTANCE)
                                .await?;
                        }

                        conflater_tx
                            .send((
                                address_point,
                                ConflateEvent::NearbyBuildings(nearby_buildings),
                                street_exists_doc,
                            ))
                            .await?;
                    }

                    if let Some(change) = change {
                        xml_writer_tx.send(change).await?;
                    }

                    processed_ctr.fetch_add(1, Ordering::Relaxed);
                }

                drop(osm_filter);
                let _ = conn_task.await?;
                Ok(())
            })
        })
        .take(num_tasks)
        .collect();

    drop(unfiltered_rx);
    drop(conflater_tx);

    let conflater_new_ctr = new_ctr.clone();
    let conflater_mod_ctr = mod_ctr.clone();
    let conflater_processed_ctr = processed_ctr.clone();
    let conflater_skip_ctr = skip_ctr.clone();

    let conflater_task: tokio::task::JoinHandle<Result<(), Box<dyn Error + Sync + Send>>> =
        tokio::spawn(async move {
            let elts_to_points = Arc::new(Mutex::new(HashMap::new()));
            let points_to_elts = Arc::new(Mutex::new(HashMap::new()));
            let mut new_address_points = Vec::new();
            let mut modified_elts = Vec::new();
            let new_ctr = conflater_new_ctr;
            let mod_ctr = conflater_mod_ctr;
            let processed_ctr = conflater_processed_ctr;
            let skip_ctr = conflater_skip_ctr;

            let print = || {
                let processed = processed_ctr.load(Ordering::Relaxed);
                let new = new_ctr.load(Ordering::Relaxed);
                let modify = mod_ctr.load(Ordering::Relaxed);
                let skipped = skip_ctr.load(Ordering::Relaxed);

                eprint!(
                    "\rnew/skipped/modified/checked/total: {}/{}/{}/{}/{}",
                    new, skipped, modify, processed, num_points
                );
            };

            'recv: while let Some((address_point, event, street_exists_doc)) =
                conflater_rx.recv().await
            {
                print();

                let node_nys_ids = address_point.nys_ids.clone();
                let mut node = Node::new(0, address_point.clone());

                if let Some(street_exists_doc) = street_exists_doc {
                    if street_exists_doc.elements.len() <= 0 {
                        node.mark_for_review("no matching street nearby");
                    }
                }

                let node = Arc::new(Mutex::new(node));
                let mut candidate_elts = Vec::new();

                match event {
                    ConflateEvent::NearbyBuildings(nearby_buildings) => {
                        let buildings_containing_address_point = nearby_buildings.ways_to_rels();

                        if buildings_containing_address_point.len() == 0 {
                            new_address_points.push(node.clone());
                            continue 'recv;
                        }

                        for (building, relation) in buildings_containing_address_point {
                            // if there's exactly one, but it's part of a multipolygon,
                            // mark for manual review
                            if let Some(_) = relation {
                                node.lock().unwrap().mark_for_review("multipolygon");
                                new_address_points.push(node.clone());
                                continue 'recv;
                            }

                            let building = Arc::new(Mutex::new(OSMElement::Way(building)));
                            candidate_elts.push(building);
                        }
                    }
                    ConflateEvent::FoundMatch(matched_elt) => {
                        let tags = matched_elt.get_tags();
                        let existing_house_num = tags.get(constants::KEY_HOUSE_NUM);
                        let existing_rooms = tags.get(constants::KEY_ROOMS);
                        let existing_unit = tags.get(constants::KEY_UNIT);
                        let existing_flats = tags.get(constants::KEY_FLATS);
                        let existing_floors = tags.get(constants::KEY_FLOOR);
                        let existing_buildings = tags.get(constants::KEY_BUILDING);
                        let existing_state = tags.get(constants::KEY_STATE);
                        let existing_postcode = tags.get(constants::KEY_POST_CODE);
                        let multi_housenum = existing_house_num.is_some()
                            && existing_house_num.unwrap().contains(";");

                        let mut addr_tags_match = match address_point.sub_address {
                            Some(SubAddress::Unit(ref unit)) => {
                                existing_unit.is_some()
                                    && parse_unit(existing_unit.unwrap()) == unit
                            }
                            Some(SubAddress::Flats(ref flats)) => {
                                existing_flats.is_some()
                                    && existing_flats
                                        .unwrap()
                                        .split(";")
                                        .map(String::from)
                                        .collect::<BTreeSet<_>>()
                                        == *flats
                            }
                            None => existing_unit.is_none() && existing_flats.is_none(),
                        };

                        addr_tags_match &= match address_point.rooms {
                            Some(ref rooms) => {
                                existing_rooms.is_some()
                                    && existing_rooms
                                        .unwrap()
                                        .split(";")
                                        .map(String::from)
                                        .collect::<BTreeSet<_>>()
                                        == *rooms
                            }
                            None => existing_rooms.is_none(),
                        };

                        addr_tags_match &= match address_point.floors {
                            Some(ref floors) => {
                                existing_floors.is_some()
                                    && existing_floors
                                        .unwrap()
                                        .split(";")
                                        .map(String::from)
                                        .collect::<BTreeSet<_>>()
                                        == *floors
                            }
                            None => existing_floors.is_none(),
                        };

                        addr_tags_match &= match address_point.buildings {
                            Some(ref buildings) => {
                                existing_buildings.is_some()
                                    && existing_buildings
                                        .unwrap()
                                        .split(";")
                                        .map(String::from)
                                        .collect::<BTreeSet<_>>()
                                        == *buildings
                            }
                            None => existing_buildings.is_none(),
                        };

                        addr_tags_match &= existing_state.is_some()
                            && existing_state.unwrap() == &address_point.state
                            && existing_postcode.is_some()
                            && existing_postcode.unwrap() == &address_point.post_code;

                        if addr_tags_match || multi_housenum {
                            skip_ctr.fetch_add(1, Ordering::Relaxed);
                            skip::log_skip(&address_point);
                            continue 'recv;
                        }

                        candidate_elts.push(Arc::new(Mutex::new(matched_elt)));
                    }
                }

                // if there are no candidates for conflation, just add as an addition and move on
                if candidate_elts.len() == 0 {
                    new_address_points.push(node);
                    continue 'recv;
                }

                for candidate_elt in candidate_elts {
                    let mut elts_to_points = elts_to_points.lock().unwrap();
                    let (_, ref mut points) = elts_to_points
                        .entry(candidate_elt.lock().unwrap().get_id())
                        .or_insert((candidate_elt.clone(), Vec::new()));
                    points.push(node.clone());

                    let mut points_to_elts = points_to_elts.lock().unwrap();
                    let (_, ref mut elts) = points_to_elts
                        .entry(node_nys_ids.clone())
                        .or_insert((node.clone(), Vec::new()));
                    elts.push(candidate_elt);
                }
            }

            // Turn the mappings into lists of changes.  At that point, only one reference to each
            // address point and way should remain, and we can unwrap them back to their values so
            // we can consume them into Changes
            for (_, (node, candidate_elts)) in points_to_elts.lock().unwrap().drain() {
                let mut node_handle = node.lock().unwrap();

                if candidate_elts.len() != 1 {
                    node_handle.mark_for_review("inside/near multiple ways");
                    drop(node_handle);
                    new_address_points.push(node.clone());
                    continue;
                }

                let candidate_elt = candidate_elts.get(0).unwrap();
                let mut candidate_elt_handle = candidate_elt.lock().unwrap();

                let mut mark_if_different = |new_key, candidate_key| {
                    if let Some(val) = node_handle.tags.get(new_key) {
                        if let Some(existing_val) =
                            candidate_elt_handle.get_tags().get(candidate_key)
                        {
                            if val != existing_val {
                                node_handle.mark_for_review(format!(
                                    "existing element's {} has different {}",
                                    candidate_key, new_key
                                ));
                                return true;
                            }
                        }
                    }

                    false
                };

                // check if the way already has different address tags
                let mut conflict =
                    mark_if_different(constants::KEY_HOUSE_NUM, constants::KEY_HOUSE_NUM);

                conflict |= mark_if_different(constants::KEY_UNIT, constants::KEY_UNIT);
                conflict |= mark_if_different(constants::KEY_FLATS, constants::KEY_FLATS);
                conflict |= mark_if_different(constants::KEY_UNIT, constants::KEY_FLATS);
                conflict |= mark_if_different(constants::KEY_FLATS, constants::KEY_UNIT);

                // don't check for conflict on street name because it might just be an apostrophe.
                // it had to pass other equivalence checks to get to this point
                //conflict |=
                //mark_if_different(constants::KEY_STREET_NAME, constants::KEY_STREET_NAME);

                // don't check for city conflicts. too many FPs
                // conflict |= mark_if_different(constants::KEY_CITY, constants::KEY_CITY);
                conflict |= mark_if_different(constants::KEY_STATE, constants::KEY_STATE);
                conflict |= mark_if_different(constants::KEY_POST_CODE, constants::KEY_POST_CODE);

                if conflict {
                    new_address_points.push(node.clone());
                    continue;
                }

                if let Some((_, nodes)) = elts_to_points
                    .lock()
                    .unwrap()
                    .get(&candidate_elt_handle.get_id())
                {
                    if nodes.len() != 1 {
                        drop(node_handle);
                        new_address_points.push(node.clone());
                        continue;
                    }
                }

                // we're ignoring city conflicts, so if there's an existing element that we're
                // filling in, and that element has a city, remove the city from the address point
                // so it doesn't overwrite the existing value
                if candidate_elt_handle
                    .get_tags()
                    .contains_key(constants::KEY_CITY)
                {
                    node_handle.tags.remove(constants::KEY_CITY);
                }

                if candidate_elt_handle
                    .get_tags()
                    .contains_key(constants::KEY_STREET_NAME)
                {
                    node_handle.tags.remove(constants::KEY_STREET_NAME);
                }

                candidate_elt_handle
                    .get_mut_tags()
                    .extend(node_handle.tags.clone());

                drop(candidate_elt_handle);
                modified_elts.push(candidate_elt.clone());
            }

            drop(elts_to_points);
            drop(points_to_elts);

            let mut seen_addresses = HashMap::new();

            for node in new_address_points.iter().map(|node| node.lock().unwrap()) {
                let address_hash = types::hash_tags_by_address(&node.tags);
                let count = seen_addresses.entry(address_hash).or_insert(0);
                *count += 1;
            }
            for way in modified_elts.iter().map(|way| way.lock().unwrap()) {
                let address_hash = types::hash_tags_by_address(&way.get_tags());
                let count = seen_addresses.entry(address_hash).or_insert(0);
                *count += 1;
            }

            new_ctr.fetch_add(new_address_points.len(), Ordering::Relaxed);
            mod_ctr.fetch_add(modified_elts.len(), Ordering::Relaxed);
            print();

            for new_node in new_address_points {
                let mut new_node = Arc::try_unwrap(new_node).unwrap().into_inner().unwrap();
                let address_hash = types::hash_tags_by_address(&new_node.tags);

                if *seen_addresses.get(&address_hash).unwrap() > 1 {
                    new_node.mark_for_review("repeat address");
                }

                xml_writer_tx.send(types::Change::New(new_node)).await?;
            }

            for elt in modified_elts {
                let mut elt = Arc::try_unwrap(elt).unwrap().into_inner().unwrap();
                let address_hash = types::hash_tags_by_address(&elt.get_tags());
                if *seen_addresses.get(&address_hash).unwrap() > 1 {
                    elt.mark_for_review("repeat address");
                }

                xml_writer_tx.send(types::Change::Modify(elt)).await?;
            }

            Ok(())
        });

    let xml_writer_task: tokio::task::JoinHandle<xml::writer::Result<()>> =
        tokio::spawn(async move {
            let mut new_id = -1;
            let mut stdout = io::stdout();
            let mut writer = EmitterConfig::new()
                .perform_indent(true)
                .create_writer(&mut stdout);

            osm::start_osm_root(&mut writer)?;

            let mut creations = Vec::new();
            let mut modifications = Vec::new();

            while let Some(change) = xml_writer_rx.recv().await {
                match change {
                    Change::New(mut node) => {
                        node.id = new_id;
                        creations.push(node);
                        new_id -= 1;
                    }
                    Change::Modify(element) => modifications.push(element),
                }
            }

            osm::start_element(&mut writer, "create")?;

            for node in creations {
                osm::write_node(&mut writer, &node)?;
            }

            osm::end_element(&mut writer)?;
            osm::start_element(&mut writer, "modify")?;

            for elt in modifications {
                osm::write_element(&mut writer, &elt)?;
            }

            osm::end_element(&mut writer)?;
            osm::end_element(&mut writer)?;

            Ok(())
        });

    send_task.await??;
    try_join_all(filter_tasks).await?;
    conflater_task.await??;
    xml_writer_task.await??;

    Ok(())
}

/// find points that are very close together and set all their coordinates equal so they get
/// collapsed
fn stack_close_points(points: &mut Vec<RefCell<AddressPoint>>) {
    debug!("Sorting by lat...");

    let mut by_x: Vec<_> = points.iter().collect();
    by_x.sort_unstable_by(|a, b| {
        a.borrow()
            .coords
            .0
            .partial_cmp(&b.borrow().coords.0)
            .unwrap()
    });

    let mut x_candidates = Vec::new();

    for i in 0..by_x.len() - 1 {
        for j in (i + 1)..by_x.len() {
            let a = geo::Point::from((by_x[i].borrow().coords.0, 0.));
            let b = geo::Point::from((by_x[j].borrow().coords.0, 0.));

            if a == b {
                continue;
            }

            if a.geodesic_distance(&b) <= constants::STACK_MAX_DISTANCE {
                if by_x[i].borrow().nys_ids <= by_x[j].borrow().nys_ids {
                    x_candidates.push((by_x[i], by_x[j]));
                } else {
                    x_candidates.push((by_x[j], by_x[i]));
                }
            } else {
                break;
            }
        }
    }

    debug!("Sorting by lon...");
    let mut by_y: Vec<_> = points.iter().collect();
    by_y.sort_unstable_by(|a, b| {
        a.borrow()
            .coords
            .1
            .partial_cmp(&b.borrow().coords.1)
            .unwrap()
    });

    let mut y_candidates = Vec::new();

    for i in 0..by_y.len() - 1 {
        for j in (i + 1)..by_y.len() {
            let a = geo::Point::from((0., by_y[i].borrow().coords.1));
            let b = geo::Point::from((0., by_y[j].borrow().coords.1));

            if a == b {
                continue;
            }

            if a.geodesic_distance(&b) <= constants::STACK_MAX_DISTANCE {
                if by_y[i].borrow().nys_ids <= by_y[j].borrow().nys_ids {
                    y_candidates.push((by_y[i], by_y[j]));
                } else {
                    y_candidates.push((by_y[j], by_y[i]));
                }
            } else {
                break;
            }
        }
    }

    debug!("Finding intersection...");
    let x_ids: BTreeSet<_> = x_candidates
        .iter()
        .map(|(a, b)| (a.borrow().nys_ids.clone(), b.borrow().nys_ids.clone()))
        .collect();

    let y_ids: BTreeSet<_> = y_candidates
        .iter()
        .map(|(a, b)| (a.borrow().nys_ids.clone(), b.borrow().nys_ids.clone()))
        .collect();

    let nearby_ids: BTreeSet<_> = x_ids.intersection(&y_ids).collect();

    debug!("Filtering down to just nearby things...");
    let mut nearby: Vec<_> = x_candidates
        .into_iter()
        .filter(|(a, b)| {
            nearby_ids.contains(&(a.borrow().nys_ids.clone(), b.borrow().nys_ids.clone()))
        })
        .collect();

    debug!("Sorting nearby points by ID...");
    nearby.sort_unstable_by(|a, b| {
        a.0.borrow()
            .nys_ids
            .cmp(&b.0.borrow().nys_ids)
            .then(a.1.borrow().nys_ids.cmp(&b.1.borrow().nys_ids))
    });

    debug!("nearby before stacking: {:#?}", nearby);

    let mut count = 0;
    for (a, b) in &nearby {
        if a.borrow().coords == b.borrow().coords {
            continue;
        }

        let a = a.borrow();
        let mut b = b.borrow_mut();

        debug!("stacked points {:?} and {:?}", a, b);
        b.coords = a.coords;
        count += 1;
    }

    debug!("nearby after stacking: {:#?}", nearby);
    info!("stacked {} points", count);
}

fn collapse_ap_btreeset<K, A, M>(
    grouped: HashMap<K, Vec<AddressPoint>>,
    mut_accessor: A,
    move_accessor: M,
) -> Vec<AddressPoint>
where
    A: Fn(&mut AddressPoint) -> &mut Option<BTreeSet<String>>,
    M: Fn(AddressPoint) -> Option<BTreeSet<String>>,
{
    grouped
        .into_iter()
        .map(|(_, orig_aps)| {
            let mut address_points = orig_aps.clone();
            let mut final_ap = address_points.pop().unwrap();

            for ap in address_points.into_iter() {
                final_ap.nys_ids.extend(ap.nys_ids.clone());

                match (mut_accessor(&mut final_ap), move_accessor(ap)) {
                    (final_btreeset_field @ &mut None, btreeset_field @ Some(_)) => {
                        *final_btreeset_field = btreeset_field
                    }
                    (&mut Some(ref mut final_btreeset_field), Some(btreeset_field)) => {
                        final_btreeset_field.extend(btreeset_field);
                    }
                    _ => (),
                }
            }

            // if the final size of the combined address point is too large,
            // just leave them separate
            if final_ap.tags_under_size_limit() {
                vec![final_ap]
            } else {
                info!(
                    "address points were too large to combine: {}: {} entries",
                    final_ap,
                    orig_aps.len()
                );
                orig_aps
            }
        })
        .flatten()
        .collect()
}

/// take every point by coordinate and address and combine them into one point if possible. Then
/// regroup by just address so every address point for the same primary address can be processed
/// together
fn collapse_units<K>(points_by_coord: HashMap<K, Vec<AddressPoint>>) -> Vec<AddressPoint> {
    points_by_coord
        .into_iter()
        .map(|(_, orig_aps)| {
            let mut address_points = orig_aps.clone();
            let mut final_ap = address_points.pop().unwrap();

            for ap in address_points.into_iter() {
                let sub_address = ap.sub_address;
                final_ap.nys_ids.extend(ap.nys_ids);

                match (&mut final_ap.sub_address, sub_address) {
                    (final_sub_address @ &mut None, unit @ Some(SubAddress::Unit(_))) => {
                        *final_sub_address = unit
                    }
                    (
                        final_sub_address @ &mut Some(SubAddress::Unit(_)),
                        Some(SubAddress::Unit(unit)),
                    ) => {
                        let old_unit =
                            final_sub_address.replace(types::SubAddress::Flats(BTreeSet::new()));
                        if let types::SubAddress::Flats(ref mut new_flats) =
                            final_sub_address.as_mut().unwrap()
                        {
                            if let types::SubAddress::Unit(final_unit) = old_unit.unwrap() {
                                new_flats.insert(final_unit);
                            } else {
                                panic!("impossible");
                            }
                            new_flats.insert(unit);
                        } else {
                            panic!("impossible");
                        }
                    }
                    (
                        final_sub_address @ &mut Some(SubAddress::Flats(_)),
                        Some(SubAddress::Unit(unit)),
                    ) => {
                        if let types::SubAddress::Flats(ref mut final_flats) =
                            final_sub_address.as_mut().unwrap()
                        {
                            final_flats.insert(unit);
                        } else {
                            panic!("impossible");
                        }
                    }
                    _ => (),
                }
            }

            if let Some(ref mut sub_address) = final_ap.sub_address {
                sub_address.collapse_flats();
            }

            // if the final size of the combined address point is too large,
            // just leave them separate
            if final_ap.tags_under_size_limit() {
                vec![final_ap]
            } else {
                info!(
                    "address points were too large to combine: {}: {} entries",
                    final_ap,
                    orig_aps.len()
                );
                orig_aps
            }
        })
        .flatten()
        .collect()
}

fn absorb_single_floors_with_units<K>(
    grouped_except_rooms_units: HashMap<K, Vec<AddressPoint>>,
) -> Vec<AddressPoint> {
    grouped_except_rooms_units
        .into_iter()
        .map(|(_, orig_aps)| {
            if orig_aps.len() == 2 {
                let mut aps = orig_aps.clone();
                let (mut first, mut second) = (aps.pop().unwrap(), aps.pop().unwrap());

                match (
                    (&first.floors, &first.rooms, &first.sub_address),
                    (&second.floors, &second.rooms, &second.sub_address),
                ) {
                    ((Some(_), None, None), (_, _, _)) => {
                        second.nys_ids.extend(first.nys_ids);
                        vec![second]
                    }
                    ((_, _, _), (Some(_), None, None)) => {
                        first.nys_ids.extend(second.nys_ids);
                        vec![first]
                    }
                    _ => orig_aps,
                }
            } else {
                orig_aps
            }
        })
        .flatten()
        .collect()
}

fn collapse_sub_address_clusters<K>(
    grouped_by_primary_address: HashMap<K, Vec<AddressPoint>>,
) -> Vec<AddressPoint> {
    grouped_by_primary_address
        .into_iter()
        .map(|(_, mut aps)| {
            let mut i = 0;

            while i < aps.len() - 1 && aps.len() > 1 {
                let mut j = i + 1;
                let api_buildings = aps[i].buildings.clone();
                let api_floors = aps[i].floors.clone();
                let api_sub_address = aps[i].sub_address.clone();
                let api_rooms = aps[i].rooms.clone();

                while j < aps.len() && aps.len() > 1 {
                    let apj_buildings = aps[j].buildings.clone();
                    let apj_floors = aps[j].floors.clone();
                    let apj_sub_address = aps[j].sub_address.clone();
                    let apj_rooms = aps[j].rooms.clone();

                    match (
                        (&api_buildings, &api_floors, &api_sub_address, &api_rooms),
                        (&apj_buildings, &apj_floors, &apj_sub_address, &apj_rooms),
                    ) {
                        ((None, None, None, None), _) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            break;
                        }
                        ((Some(_), None, None, None), (None, _, _, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].buildings = api.buildings;
                            break;
                        }
                        ((Some(_), Some(_), None, None), (None, None, _, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].buildings = api.buildings;
                            aps[j].floors = api.floors;
                            break;
                        }
                        ((Some(_), Some(_), Some(_), None), (None, None, None, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].buildings = api.buildings;
                            aps[j].floors = api.floors;
                            aps[j].sub_address = api.sub_address;
                            break;
                        }
                        ((Some(_), Some(_), Some(_), Some(_)), (None, None, None, None)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].buildings = api.buildings;
                            aps[j].floors = api.floors;
                            aps[j].sub_address = api.sub_address;
                            aps[j].rooms = api.rooms;
                            break;
                        }
                        ((None, Some(_), None, None), (_, None, _, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].floors = api.floors;
                            break;
                        }
                        ((None, Some(_), Some(_), None), (_, None, None, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].floors = api.floors;
                            aps[j].sub_address = api.sub_address;
                            break;
                        }
                        ((None, Some(_), Some(_), Some(_)), (_, None, None, None)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].floors = api.floors;
                            aps[j].sub_address = api.sub_address;
                            aps[j].rooms = api.rooms;
                            break;
                        }
                        ((None, None, Some(_), None), (_, _, None, _)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].sub_address = api.sub_address;
                            break;
                        }
                        ((None, None, Some(_), Some(_)), (_, _, None, None)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].sub_address = api.sub_address;
                            aps[j].rooms = api.rooms;
                            break;
                        }
                        ((None, None, None, Some(_)), (_, _, _, None)) => {
                            let api = aps.remove(i);
                            j -= 1;
                            aps[j].nys_ids.extend(api.nys_ids);
                            aps[j].rooms = api.rooms;
                            break;
                        }
                        _ => {
                            j += 1;
                        }
                    }
                }

                i += 1;
            }

            aps
        })
        .flatten()
        .collect()
}
