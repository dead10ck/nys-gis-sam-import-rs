use std::collections::hash_map::DefaultHasher;
use std::collections::{BTreeSet, HashMap};
use std::error::Error;
use std::fmt::{self, Display};
use std::hash::Hash;
use std::hash::Hasher;
use std::str::FromStr;

use gdal;
use gdal::spatial_ref::CoordTransform;
use log::debug;
use regex::Regex;

use crate::constants;

pub(crate) fn parse_unit(unit: &str) -> &str {
    unit.trim_end().split_whitespace().last().unwrap_or(unit)
}

pub(crate) type LatLon = (f64, f64);

impl From<&JsonLatLon> for LatLon {
    fn from(latlon: &JsonLatLon) -> Self {
        (latlon.lat, latlon.lon)
    }
}

#[derive(Debug, Clone)]
pub(crate) struct JsonLatLon {
    lat: f64,
    lon: f64,
}

#[derive(Debug, Clone)]
pub(crate) struct AddressPoint {
    pub nys_ids: Vec<String>,
    pub coords: LatLon,
    pub prefix_address_num: Option<String>,
    pub house_num: i32,
    pub suffix_address_num: Option<String>,
    pub street_name: String,
    pub sub_address: Option<SubAddress>,
    pub floors: Option<BTreeSet<String>>,
    pub buildings: Option<BTreeSet<String>>,
    pub rooms: Option<BTreeSet<String>>,
    pub city: String,
    pub state: String,
    pub post_code: String,
    pub location: Option<String>,
}

impl AddressPoint {
    pub fn from_feature<'a>(
        mut feature: gdal::vector::Feature<'a>,
        transform: &CoordTransform,
    ) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let (lat, lon) = get_feature_point(&mut feature, transform)?;

        let house_num = feature
            .field("AddressNumber")?
            .ok_or("AddressNumber missing")?
            .into_int()
            .ok_or("AddressNumber not a number")?;

        let sub_address = feature.field("Unit")?.map(|unit| {
            let unit = unit.into_string().ok_or("Unit not a string")?;
            Ok(SubAddress::Unit(parse_unit(&unit).into()))
        });

        let sub_address = match sub_address {
            None => None,
            Some(Err(e)) => return Err(e),
            Some(Ok(s)) => Some(s),
        };

        let nys_id = get_property_string("NYSAddressPointID", &mut feature)?.unwrap();

        debug!("Parsing address point: {}", nys_id);

        let prefix_address_num = get_property_string("PrefixAddressNumber", &mut feature)?;
        let suffix_address_num = get_property_string("SuffixAddressNumber", &mut feature)?;
        let street_name = get_property_string("CompleteStreetName", &mut feature)?
            .ok_or("missing completestreetname")?;
        let building = get_property_string("Building", &mut feature)?;
        let floor = get_property_string("Floor", &mut feature)?;
        let room = get_property_string("Room", &mut feature)?;
        let location = get_property_string("Location", &mut feature)?;
        let city = get_property_string("ZipName", &mut feature)?.ok_or("missing ZipName")?;
        let state = get_property_string("State", &mut feature)?.ok_or("missing State")?;
        let post_code = get_property_string("ZipCode", &mut feature)?.ok_or("missing ZipCode")?;

        Ok(AddressPoint {
            nys_ids: vec![nys_id],
            coords: (lat, lon),
            prefix_address_num,
            house_num,
            suffix_address_num,
            sub_address,
            buildings: building.map(|b| {
                let mut buildings = BTreeSet::new();
                buildings.insert(b);
                buildings
            }),
            floors: floor.map(|f| {
                let mut floors = BTreeSet::new();
                floors.insert(f);
                floors
            }),
            rooms: room.map(|r| {
                let mut rooms = BTreeSet::new();
                rooms.insert(r);
                rooms
            }),
            street_name,
            city,
            state,
            post_code,
            location,
        })
    }

    pub fn full_address_num(&self) -> String {
        let mut address_num = String::new();

        if let Some(prefix) = &self.prefix_address_num {
            address_num.push_str(prefix);
            address_num.push_str(" ");
        }

        address_num.push_str(&self.house_num.to_string());

        if let Some(suffix) = &self.suffix_address_num {
            address_num.push_str(" ");
            address_num.push_str(suffix);
        }

        address_num
    }

    pub fn hash_all_except_floor(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.sub_address.hash(&mut hasher);
        self.buildings.hash(&mut hasher);
        self.rooms.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_all_except_location_no_coords(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.sub_address.hash(&mut hasher);
        self.buildings.hash(&mut hasher);
        self.rooms.hash(&mut hasher);
        self.floors.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_all_except_building(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.floors.hash(&mut hasher);
        self.sub_address.hash(&mut hasher);
        self.rooms.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_all_except_sub_address(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.buildings.hash(&mut hasher);
        self.floors.hash(&mut hasher);
        self.rooms.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_all_except_room(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.buildings.hash(&mut hasher);
        self.floors.hash(&mut hasher);
        self.sub_address.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_all_except_room_unit(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.buildings.hash(&mut hasher);
        self.floors.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn hash_by_coord_primary_address(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.coords.0.to_bits().hash(&mut hasher);
        self.coords.1.to_bits().hash(&mut hasher);
        self.prefix_address_num.hash(&mut hasher);
        self.house_num.hash(&mut hasher);
        self.suffix_address_num.hash(&mut hasher);
        self.street_name.hash(&mut hasher);
        self.city.hash(&mut hasher);
        self.state.hash(&mut hasher);
        self.post_code.hash(&mut hasher);
        hasher.finish()
    }

    pub fn tags_under_size_limit(&self) -> bool {
        let flats_size = match &self.sub_address {
            Some(SubAddress::Flats(flats)) => {
                flats.iter().map(|id| id.len()).sum::<usize>() + self.nys_ids.len() - 1
            }
            _ => 0,
        };

        let buildings_size = match &self.buildings {
            Some(buildings) => {
                buildings.iter().map(|f| f.len()).sum::<usize>() + buildings.len() - 1
            }
            _ => 0,
        };

        let floors_size = match &self.floors {
            Some(floors) => floors.iter().map(|f| f.len()).sum::<usize>() + floors.len() - 1,
            _ => 0,
        };

        let rooms_size = match &self.rooms {
            Some(rooms) => rooms.iter().map(|f| f.len()).sum::<usize>() + rooms.len() - 1,
            _ => 0,
        };

        flats_size <= constants::TAG_SIZE_LIMIT
            && buildings_size <= constants::TAG_SIZE_LIMIT
            && floors_size <= constants::TAG_SIZE_LIMIT
            && rooms_size <= constants::TAG_SIZE_LIMIT
    }

    pub fn group_by_hasher<I, F>(points: I, hash: F) -> HashMap<u64, Vec<Self>>
    where
        I: IntoIterator<Item = Self>,
        F: Fn(&Self) -> u64,
    {
        let mut groups = HashMap::new();

        for address_point in points.into_iter() {
            let key = hash(&address_point);
            let point_entry = groups.entry(key).or_insert(Vec::new());
            point_entry.push(address_point);
        }

        groups
    }

    pub fn has_excluded_street(&self) -> bool {
        let state_route_pattern = Regex::new(constants::STATE_ROUTE_PATTERN).unwrap();
        state_route_pattern.is_match(&self.street_name)
    }
}

impl Display for AddressPoint {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let mut address_str = format!("{} {}", self.house_num, self.street_name);

        if let Some(sub_address) = &self.sub_address {
            address_str.push_str(&format!(" {}", sub_address));
        }

        address_str.push_str(&format!(
            ", {}, {} {}",
            self.city, self.state, self.post_code
        ));
        fmt.write_str(&address_str)?;

        Ok(())
    }
}

#[derive(Debug, Clone, Hash)]
pub(crate) enum SubAddress {
    Unit(String),
    Flats(BTreeSet<String>),
}

impl Display for SubAddress {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            SubAddress::Unit(unit) => {
                fmt.write_str(unit)?;
            }
            SubAddress::Flats(flats) => {
                let flats_str = flats
                    .iter()
                    .map(AsRef::as_ref)
                    .collect::<Vec<_>>()
                    .join(";");

                fmt.write_str(&flats_str)?;
            }
        };

        Ok(())
    }
}

impl SubAddress {
    pub fn collapse_flats(&mut self) {
        if let SubAddress::Flats(ref mut flats) = self {
            let mut low: usize = 0;
            let mut high: usize = 0;
            let mut new_flats = BTreeSet::new();

            let flats_nums: Result<BTreeSet<usize>, _> =
                flats.iter().map(|s| usize::from_str(s)).collect();

            let flats_nums = if let Ok(flats_nums) = flats_nums {
                flats_nums
            } else {
                return;
            };

            let emit_flat_range = |low, high, new_flats: &mut BTreeSet<_>| {
                let flat_range = if high > low {
                    format!("{}-{}", low, high)
                } else {
                    format!("{}", low)
                };

                new_flats.insert(flat_range);
            };

            for num in flats_nums.into_iter() {
                if low == 0 && high == 0 {
                    low = num;
                    high = num;
                } else if num == high + 1 {
                    high = num;
                } else if low != 0 && high != 0 {
                    emit_flat_range(low, high, &mut new_flats);
                    low = num;
                    high = num;
                }
            }

            emit_flat_range(low, high, &mut new_flats);

            debug!("collapsing units {:?} to {:?}", flats, new_flats);

            std::mem::swap(flats, &mut new_flats);
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct Node {
    pub(crate) id: i64,
    pub(crate) version: Option<i32>,
    pub(crate) lat: f64,
    pub(crate) lon: f64,
    pub(crate) tags: HashMap<String, String>,
}

impl Node {
    pub fn new(id: i64, address_point: AddressPoint) -> Self {
        let mut tags = HashMap::new();
        let address_num = address_point.full_address_num();

        tags.insert(constants::KEY_HOUSE_NUM.into(), address_num);
        tags.insert(constants::KEY_STREET_NAME.into(), address_point.street_name);
        tags.insert(constants::KEY_CITY.into(), address_point.city);
        tags.insert(constants::KEY_STATE.into(), address_point.state);
        tags.insert(constants::KEY_POST_CODE.into(), address_point.post_code);

        let mut nys_id_tag_value = String::new();
        let mut nys_id_tag_prefix_num = 0;

        let mut emit = |value, prefix_num: usize| {
            let key = if prefix_num > 0 {
                String::from(constants::KEY_NYS_ID) + ":" + &prefix_num.to_string()
            } else {
                String::from(constants::KEY_NYS_ID)
            };

            tags.insert(key, value);
        };

        for nys_id in address_point.nys_ids {
            let new_size = nys_id_tag_value.len() + nys_id.len() + 1;

            if new_size > constants::TAG_SIZE_LIMIT {
                let value = nys_id_tag_value;
                nys_id_tag_value = String::new();
                emit(value, nys_id_tag_prefix_num);
                nys_id_tag_prefix_num += 1;
            }

            if nys_id_tag_value.len() > 0 {
                nys_id_tag_value.push(';');
            }

            nys_id_tag_value.push_str(&nys_id);
        }

        if nys_id_tag_value.len() > 0 {
            emit(nys_id_tag_value, nys_id_tag_prefix_num);
        }

        if let Some(sub_address) = address_point.sub_address {
            match sub_address {
                SubAddress::Unit(unit) => tags.insert(constants::KEY_UNIT.into(), unit),
                SubAddress::Flats(flats) => tags.insert(
                    constants::KEY_FLATS.into(),
                    flats.into_iter().collect::<Vec<_>>().join(";"),
                ),
            };
        }

        if let Some(rooms) = address_point.rooms {
            tags.insert(
                constants::KEY_ROOMS.into(),
                rooms.into_iter().collect::<Vec<_>>().join(";"),
            );
        }

        if let Some(floors) = address_point.floors {
            tags.insert(
                constants::KEY_FLOOR.into(),
                floors.into_iter().collect::<Vec<_>>().join(";"),
            );
        }

        if let Some(buildings) = address_point.buildings {
            tags.insert(
                constants::KEY_BUILDING.into(),
                buildings.into_iter().collect::<Vec<_>>().join(";"),
            );
        }

        Node {
            id,
            version: None,
            lat: address_point.coords.0,
            lon: address_point.coords.1,
            tags,
        }
    }

    pub(crate) fn mark_for_review<S: Into<String>>(&mut self, reason: S) {
        self.tags
            .insert(String::from(constants::KEY_REVIEW), reason.into());
    }
}

#[derive(Debug, Clone)]
pub(crate) struct Way {
    pub(crate) id: i64,
    pub(crate) version: Option<i32>,
    pub(crate) nodes: Vec<i64>,
    pub(crate) tags: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub(crate) struct Relation {
    pub(crate) id: i64,
    pub(crate) version: Option<i32>,
    pub(crate) members: Vec<RelationMember>,
    pub(crate) tags: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub(crate) struct RelationMember {
    pub(crate) refid: i64,
    pub(crate) osm_type: String,
    pub(crate) role: String,
}

#[derive(Debug, Clone)]
pub(crate) enum OSMElement {
    Node(Node),
    Way(Way),
    Relation(Relation),
}

impl OSMElement {
    pub(crate) fn get_tags(&self) -> &HashMap<String, String> {
        match self {
            OSMElement::Node(node) => &node.tags,
            OSMElement::Way(way) => &way.tags,
            OSMElement::Relation(rel) => &rel.tags,
        }
    }

    pub(crate) fn get_mut_tags(&mut self) -> &mut HashMap<String, String> {
        match self {
            OSMElement::Node(node) => &mut node.tags,
            OSMElement::Way(way) => &mut way.tags,
            OSMElement::Relation(rel) => &mut rel.tags,
        }
    }

    pub(crate) fn get_id(&self) -> i64 {
        match self {
            OSMElement::Node(node) => node.id,
            OSMElement::Way(way) => way.id,
            OSMElement::Relation(rel) => rel.id,
        }
    }

    pub(crate) fn mark_for_review<S: AsRef<str>>(&mut self, reason: S) {
        let review_entry = self
            .get_mut_tags()
            .entry(constants::KEY_REVIEW.into())
            .or_insert("".into());

        if review_entry.len() != 0 {
            review_entry.push(';');
        }

        review_entry.push_str(reason.as_ref());
    }
}

#[derive(Debug, Clone)]
pub(crate) struct OSMDocument {
    pub(crate) elements: Vec<OSMElement>,
}

impl OSMDocument {
    pub(crate) fn ways_to_rels(self) -> Vec<(Way, Option<i64>)> {
        let mut ways_to_relations = HashMap::new();
        let mut candidate_ways = Vec::<(Way, Option<isize>)>::new();

        for elt in self.elements {
            match elt {
                OSMElement::Way(way) => candidate_ways.push((way, None)),

                OSMElement::Relation(rel) => {
                    for member in &rel.members {
                        if member.osm_type == "way" {
                            ways_to_relations.insert(member.refid, rel.id);
                        }
                    }
                }

                _ => (),
            }
        }

        debug!("candidate ways: {:?}", candidate_ways);

        candidate_ways
            .into_iter()
            .map(|(way, _)| {
                let id = way.id;
                (way, ways_to_relations.remove(&id))
            })
            .collect()
    }
}

#[derive(Debug)]
pub(crate) enum Change {
    New(Node),
    Modify(OSMElement),
}

#[derive(Debug)]
pub(crate) enum ConflateEvent {
    FoundMatch(OSMElement),
    NearbyBuildings(OSMDocument),
}

pub(crate) fn get_property_string(
    key: &str,
    feature: &mut gdal::vector::Feature,
) -> Result<Option<String>, Box<dyn Error + Send + Sync>> {
    match feature.field(key)? {
        None => Ok(None),
        Some(val) => Ok(Some(
            val.into_string()
                .ok_or(format!("key '{}' is not a string", key))?,
        )),
    }
}

fn get_feature_point(
    feature: &mut gdal::vector::Feature,
    transform: &CoordTransform,
) -> Result<(f64, f64), Box<dyn Error + Send + Sync>> {
    let mut points = feature.geometry().transform(transform)?.get_point_vec();

    if points.len() != 1 {
        return Err("Should only be one point".into());
    }

    let (lat, lon, _) = points.pop().unwrap();

    Ok((lat, lon))
}

pub(crate) fn hash_tags_by_address(tags: &HashMap<String, String>) -> u64 {
    let mut hasher = DefaultHasher::new();
    tags.get(constants::KEY_HOUSE_NUM).hash(&mut hasher);
    tags.get(constants::KEY_STREET_NAME).hash(&mut hasher);
    tags.get(constants::KEY_ROOMS).hash(&mut hasher);
    tags.get(constants::KEY_UNIT).hash(&mut hasher);
    tags.get(constants::KEY_FLATS).hash(&mut hasher);
    tags.get(constants::KEY_FLOOR).hash(&mut hasher);
    tags.get(constants::KEY_BUILDING).hash(&mut hasher);
    tags.get(constants::KEY_CITY).hash(&mut hasher);
    tags.get(constants::KEY_STATE).hash(&mut hasher);
    tags.get(constants::KEY_POST_CODE).hash(&mut hasher);
    hasher.finish()
}
