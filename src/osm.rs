use std::io::prelude::*;

use xml::writer::{EventWriter, XmlEvent};

use crate::types::*;

pub(crate) fn start_osm_root<W: Write>(writer: &mut EventWriter<W>) -> xml::writer::Result<()> {
    let generator: String = format!("{}/{}", &super::APP_NAME, &super::VERSION);
    let event: XmlEvent = XmlEvent::start_element("osmChange")
        .attr("generator", &generator)
        .attr("version", "0.6")
        .into();
    writer.write(event)
}

pub(crate) fn write_element<W: Write>(
    writer: &mut EventWriter<W>,
    element: &OSMElement,
) -> xml::writer::Result<()> {
    match element {
        OSMElement::Node(node) => write_node(writer, node),
        OSMElement::Way(way) => write_way(writer, way),
        OSMElement::Relation(relation) => write_relation(writer, relation),
    }
}

pub(crate) fn write_node<W: Write>(
    writer: &mut EventWriter<W>,
    node: &Node,
) -> xml::writer::Result<()> {
    let id = node.id.to_string();
    let lat = node.lat.to_string();
    let lon = node.lon.to_string();

    let mut event = XmlEvent::start_element("node")
        .attr("id", &id)
        .attr("lat", &lat)
        .attr("lon", &lon);

    let version = if let Some(ver) = node.version.map(|v| v.to_string()) {
        ver
    } else {
        String::from("")
    };

    if version.len() > 0 {
        event = event.attr("version", &version);
    }

    let event: XmlEvent = event.into();
    writer.write(event)?;

    for (k, v) in &node.tags {
        write_tag(writer, k, v)?;
    }

    end_element(writer)
}

pub(crate) fn write_way<W: Write>(
    writer: &mut EventWriter<W>,
    way: &Way,
) -> xml::writer::Result<()> {
    let id = way.id.to_string();

    let mut event = XmlEvent::start_element("way").attr("id", &id);

    let version = if let Some(ver) = way.version.map(|v| v.to_string()) {
        ver
    } else {
        String::from("")
    };

    if version.len() > 0 {
        event = event.attr("version", &version);
    }

    let event: XmlEvent = event.into();
    writer.write(event)?;

    for nref in way.nodes.iter().map(ToString::to_string) {
        let event: XmlEvent = XmlEvent::start_element("nd").attr("ref", &nref).into();

        writer.write(event)?;
        end_element(writer)?;
    }

    for (k, v) in &way.tags {
        write_tag(writer, k, v)?;
    }

    end_element(writer)
}

pub(crate) fn write_relation<W: Write>(
    writer: &mut EventWriter<W>,
    relation: &Relation,
) -> xml::writer::Result<()> {
    let id = relation.id.to_string();

    let mut event = XmlEvent::start_element("relation")
        .attr("id", &id);

    let version = if let Some(ver) = relation.version.map(|v| v.to_string()) {
        ver
    } else {
        String::from("")
    };

    if version.len() > 0 {
        event = event.attr("version", &version);
    }

    let event: XmlEvent = event.into();

    writer.write(event)?;

    for member in &relation.members {
        let refid = member.refid.to_string();
        let event: XmlEvent = XmlEvent::start_element("member")
            .attr("type", &member.osm_type)
            .attr("ref", &refid)
            .attr("role", &member.role)
            .into();

        writer.write(event)?;
        end_element(writer)?;
    }

    for (k, v) in &relation.tags {
        write_tag(writer, k, v)?;
    }

    end_element(writer)
}

pub(crate) fn write_tag<W: Write>(
    writer: &mut EventWriter<W>,
    key: &str,
    value: &str,
) -> xml::writer::Result<()> {
    let event: XmlEvent = XmlEvent::start_element("tag")
        .attr("k", key)
        .attr("v", value)
        .into();

    writer.write(event)?;
    end_element(writer)
}

pub(crate) fn start_element<W: Write>(
    writer: &mut EventWriter<W>,
    name: &str,
) -> xml::writer::Result<()> {
    let event: XmlEvent = XmlEvent::start_element(name).into();
    writer.write(event)
}

pub(crate) fn end_element<W: Write>(writer: &mut EventWriter<W>) -> xml::writer::Result<()> {
    let event: XmlEvent = XmlEvent::end_element().into();
    writer.write(event)
}
