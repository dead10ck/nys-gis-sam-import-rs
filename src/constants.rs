pub(crate) const KEY_HOUSE_NUM: &'static str = "addr:housenumber";
pub(crate) const KEY_STREET_NAME: &'static str = "addr:street";
pub(crate) const KEY_UNIT: &'static str = "addr:unit";
pub(crate) const KEY_FLATS: &'static str = "addr:flats";
pub(crate) const KEY_ROOMS: &'static str = "addr:door";
pub(crate) const KEY_FLOOR: &'static str = "addr:floor";
pub(crate) const KEY_BUILDING: &'static str = "addr:housename";
pub(crate) const KEY_CITY: &'static str = "addr:city";
pub(crate) const KEY_STATE: &'static str = "addr:state";
pub(crate) const KEY_POST_CODE: &'static str = "addr:postcode";

pub(crate) const KEY_NYS_ID: &'static str = "nysgissam:nysaddresspointid";
pub(crate) const KEY_REVIEW: &'static str = "nysgissam:review";

pub(crate) const COL_OSM_ID: &'static str = "osm_id";
pub(crate) const COL_VERSION: &'static str = "osm_version";
pub(crate) const COL_WAY: &'static str = "way";
pub(crate) const COL_NODES: &'static str = "nodes";
pub(crate) const COL_MEMBERS: &'static str = "members";
pub(crate) const COL_TAGS: &'static str = "tags";

pub(crate) const EXTRA_TAG_CHANGESET: &'static str = "osm_changeset";
pub(crate) const EXTRA_TAG_TIMESTAMP: &'static str = "osm_timestamp";
pub(crate) const EXTRA_TAG_UID: &'static str = "osm_uid";
pub(crate) const EXTRA_TAG_USER: &'static str = "osm_user";
pub(crate) const EXTRA_TAG_VERSION: &'static str = "osm_version";
pub(crate) const EXTRA_TAG_WAY_AREA: &'static str = "way_area";

pub(crate) const TAG_SIZE_LIMIT: usize = 255;
pub(crate) const STACK_MAX_DISTANCE: f64 = 2.;
pub(crate) const NEARBY_MAX_DISTANCE: f64 = 1.0;

pub(crate) const STATE_ROUTE_PATTERN: &'static str =
    r"((?i:state|county|highway) )?(?i:highway|route|road) \d+";

pub(crate) const NUMBERED_STREET_PATTERN: &'static str = r"\d+";
