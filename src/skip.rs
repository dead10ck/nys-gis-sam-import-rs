use log::info;

use crate::types;

pub(crate) fn log_skip(address_point: &types::AddressPoint) {
    info!("{}", address_point);
}
