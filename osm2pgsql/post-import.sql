CREATE INDEX IF NOT EXISTS point_address ON nysgissam_point ("addr:housenumber", "addr:street");


CREATE INDEX IF NOT EXISTS polygon_address ON nysgissam_polygon ("addr:housenumber", "addr:street");


CREATE MATERIALIZED VIEW IF NOT EXISTS nysids AS
SELECT DISTINCT nysgissam_polygon.osm_id,
                regexp_split_to_table(tags->KEY, ';') AS nysid
FROM nysgissam_polygon
JOIN
  (SELECT osm_id,
          KEY
   FROM
     (SELECT osm_id,
             skeys(tags) AS KEY
      FROM nysgissam_polygon) AS keyt
   WHERE KEY like 'nysgissam:nysaddresspointid:%') AS with_keys ON nysgissam_polygon.osm_id = with_keys.osm_id
UNION
SELECT DISTINCT nysgissam_point.osm_id,
                regexp_split_to_table(tags->KEY, ';') AS nysid
FROM nysgissam_point
JOIN
  (SELECT osm_id,
          KEY
   FROM
     (SELECT osm_id,
             skeys(tags) AS KEY
      FROM nysgissam_point) AS keyt2
   WHERE KEY like 'nysgissam:nysaddresspointid%') AS with_keys_2 ON nysgissam_point.osm_id = with_keys_2.osm_id;


CREATE INDEX IF NOT EXISTS nysid_idx ON nysids (nysid);


CREATE MATERIALIZED VIEW IF NOT EXISTS rel_members AS
SELECT DISTINCT unnest(parts) AS member_id,
                id AS rel_id
FROM nysgissam_rels
ORDER BY member_id;


CREATE INDEX IF NOT EXISTS rel_members_idx ON rel_members (member_id);


CREATE INDEX IF NOT EXISTS nysgissam_line_highway_idx ON nysgissam_line(highway);


CREATE INDEX IF NOT EXISTS nysgissam_roads_highway_idx ON nysgissam_roads(highway);


CREATE INDEX IF NOT EXISTS nysgissam_roads_name_idx ON nysgissam_line ((lower(regexp_replace(name, '''', ''))));


CREATE INDEX IF NOT EXISTS nysgissam_line_name_idx ON nysgissam_line ((lower(regexp_replace(name, '''', ''))));


CREATE INDEX polygon_geog_idx ON nysgissam_polygon USING gist(cast(way AS geography));

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA PUBLIC TO osmuser;

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA PUBLIC TO osmuser;
