# New York State Address Point Import

This repo contains the source code to the program that converts
the GDB published by the state and into .osc files. It makes use
of [osm2pgsql](https://osm2pgsql.org/) and Postgres/PostGIS to filter out
addresses that already exist, and potentially conflate with
existing buildings.

## Install

First, you must install the GDAL library on your system:

https://gdal.org/download.html

Then install the Rust toolchain with [Rustup](https://rustup.rs/) or your
package manager of choice. Clone this source code and do:

```sh
cargo install --path .
```

Then you must set up a Postgres server with PostGIS, and install osm2pgsql. There's a style file in
the osm2pgsql folder that defines the schema, as well as a few scripts for bootstrapping a usable
database. Once you have Postgres up and running with the hstore and PostGIS extensions, you can
download the latest [Geofabrik dump of New
York](https://download.geofabrik.de/north-america/us/new-york.html) and run the `import` script
(you may need to modify the user and/or DB name depending on how you set up your Postgres table).
e.g.

```sh
./osm2pgsql/import new-york-latest.osm.pbf
```

You can update it by downloading updates with pyosmium-get-changes, and then adding `update`:

```sh
pyosmium-get-changes -f seq --format osc -o diffs.osc --server http://download.geofabrik.de/north-america/us/new-york-updates -vv
./osm2pgsql/import diffs.osc update
```

## Usage

First download the dataset:
https://gis.ny.gov/gisdata/inventories/details.cfm?DSID=921

Then run with a reasonable number of concurrent tasks. e.g.

```sh
./target/release/nys-gis-sam-import-rs \
   /path/to/SAM_Master_Statewide_Database.gdb \
    -t 4 \
    -b x1 y1 x2 y2 x3 y3 ... x1 y1 \
    -c 'host=localhost user=osmuser dbname=osm' \
    > result.osc
```

It will output an .osc file to stdout, which you can redirect to
a file, and also keep a separate log file that logs all the
addresses that it found were in OSM already and skipped.

It will output a running counter of the changes, and any errors
should they occur. If it encounters any errors, it will automatically retry these requests
with an exponential backoff strategy for a maximum of 15 minutes
per failing request.

It's also worth noting that the conflation phase happens after
every address is checked against Postgres, so most additions and modifications
do not happen until the very end.
